﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Scoreboards : MonoBehaviour
{
    // Start is called before the first frame update
    private void Start()
    {
        float sizeX = GetComponent<RectTransform>().rect.width;
        float sizeY = GetComponent<RectTransform>().rect.height;
        Debug.Log("X = " + sizeX + " , Y = " + sizeY);
    }
    void Update()
    {
        float camHalfHeight = Camera.main.orthographicSize;
        float camHalfWidth = Camera.main.aspect * camHalfHeight;

        float sizeX = GetComponent<RectTransform>().rect.width;
        float sizeY = GetComponent<RectTransform>().rect.height;
        // Устанавливаем новый вектор в верхний левый угол 
        Vector3 topLeftPosition = new Vector3(-camHalfWidth, camHalfHeight, 0) + Camera.main.transform.position;

        // Устанавливаем смещение на основе размера объекта
        topLeftPosition += new Vector3(400, -300, 0);

        transform.position = topLeftPosition;
    }
}
