﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class RadiusCheker : MonoBehaviour
{
    MagnetController MagnetParent;
    CircleCollider2D Collider;
    void Start()
    {
        MagnetParent = transform.parent.GetComponent<MagnetController>();
        Collider = GetComponent<CircleCollider2D>();
    }

    void Update()
    {
        Collider.radius = MagnetParent.Radius;
    }
}
