﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System;

public class FlipperInput : MonoBehaviour
{
    [SerializeField] private GameObject FlipperLeft;
    [SerializeField] private GameObject FlipperRight;
    [SerializeField] private PressedButton btnLeft;
    [SerializeField] private PressedButton btnRight;
    [SerializeField] private Button pause;

    [SerializeField] private Text timerMin1;
    [SerializeField] private Text timerMin2;
    [SerializeField] private Text timerSec1;
    [SerializeField] private Text timerSec2;
    [SerializeField] private int timerMin1Int;
    [SerializeField] private int timerMin2Int;
    [SerializeField] private int timerSec1Int;
    [SerializeField] private float timerSec2Int;
    [SerializeField] private Text levelText;

    [SerializeField] private GameObject buttonMenu;


    FlipperController LeftController, RightController;
    void Start()
    {
        Time.timeScale = 1;
        LeftController = FlipperLeft.GetComponent<FlipperController>();
        RightController = FlipperRight.GetComponent<FlipperController>();

        timerMin2Int = PlayerPrefs.GetInt("Timer") + 1;

        int level = PlayerPrefs.GetInt("Level") + 1;
        levelText.text = level.ToString();

    }

    // Update is called once per frame
    void Update()
    {
        /*if (Input.GetKey(KeyCode.Mouse0))
        {
            Vector3 mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            if (mousePos.x < 0)
            {
                LeftController.Push();
            }
            else
            {
                RightController.Push();
            }
        }*/
        if (btnLeft.IsPressed)
        {
            LeftController.Push();
        }
        if (btnRight.IsPressed)
        {
            RightController.Push();
        }

        
        
        timerSec2Int -= Time.deltaTime;
        if (timerSec2Int < 0)
        {
            timerSec2Int = 0;
            Timer();
        }
        if (timerMin1Int == 0 && timerMin2Int == 0 && timerSec1Int == 0 && timerSec2Int == 0)
        {
           
        }
        else
        {
            timerMin1.text = timerMin1Int.ToString();
            timerMin2.text = timerMin2Int.ToString();
            timerSec1.text = timerSec1Int.ToString();
            timerSec2.text = Mathf.Ceil(timerSec2Int - 1).ToString();
        }
#if UNITY_EDITOR
        if (Input.GetKey(KeyCode.A))
        {
            LeftController.Push();

        }
        if (Input.GetKey(KeyCode.D))
        {
            RightController.Push();

        }
#endif
    }

    private void Timer()
    {
        
        
        timerSec1Int--;
        timerSec2Int = 9.999999F;
        if (timerSec1Int < 0)
        {
            timerSec1Int = 5;
            if (timerMin2Int > 0)
            {
                
                timerMin2Int--;
            }
            else
            {
                if (timerMin1Int > 0)
                {
                    timerMin2Int = 9;
                    timerMin1Int--;
                }
                else
                {
                    timerMin1.gameObject.SetActive(false);
                    timerMin2.gameObject.SetActive(false);
                    timerSec1.gameObject.SetActive(false);
                    timerSec2.gameObject.SetActive(false);
                    Time.timeScale = 0;
                    SceneManager.LoadScene(0);
                }
            }
        }



       





    }

    public void OnClickPause() 
    {
        if (Time.timeScale == 0)
        {
            buttonMenu.gameObject.SetActive(false);
            Time.timeScale = 1;
        }
        else if (Time.timeScale == 1)
        {
            Time.timeScale = 0;
            buttonMenu.gameObject.SetActive(true);
        }
        

    }

    public void OnClickMenu()
    {
        SceneManager.LoadScene(0);
    }

}
