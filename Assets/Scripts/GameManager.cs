﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using System.Linq;

public class GameManager : MonoBehaviour
{
    public Sprite SoundOn;
    public Sprite SoundOff;
    public Image SoundState;
    public int progressLevel;

    public Toggle[] LevelToggles;
    public Toggle[] TimeToggles;

    [SerializeField] private GameObject mainMenu;
    [SerializeField] private GameObject settings;

    [SerializeField] private Text levelText;
    [SerializeField] private Text timerText;

    void Start()
    {
        /*int level = PlayerPrefs.GetInt("Level") + 1;
        levelText.text = level.ToString();

        int timer = PlayerPrefs.GetInt("Timer") + 1;
        timerText.text = level.ToString();*/

        if (SceneManager.GetActiveScene().name == "Main_Menu")
        {
            CheckSoundState();

            if (PlayerPrefs.HasKey("Timer"))
            {
                CheckTimerState(PlayerPrefs.GetInt("Timer"));
            }

            else
            {
                PlayerPrefs.SetInt("Timer", 0);
                CheckTimerState(0);
            }

            if (PlayerPrefs.HasKey("Level"))
            {
                CheckLevelState(PlayerPrefs.GetInt("Level"));
            }

            else
            {
                PlayerPrefs.SetInt("Level", 0);
                CheckLevelState(0);
            }
        }
    }

    


    //провкяет звук вкл. или выкл. и поставит правильное спрайт
    void CheckSoundState()
    {
        if (PlayerPrefs.GetString("Sound") != "No")
        {
            if (SceneManager.GetActiveScene().name == "Settings")
            {
                SoundState.sprite = SoundOn;
            }
        }

        else
        {
            if (SceneManager.GetActiveScene().name == "Settings")
            {
                SoundState.sprite = SoundOff;
            }
        }
    }

    //кнопка для вкл/выкл звука
    public void SoundOnOff()
    {
        if (PlayerPrefs.GetString("Sound") != "No")
        {
            PlayerPrefs.SetString("Sound", "No");
            SoundState.sprite = SoundOff;
        }

        else
        {
            PlayerPrefs.SetString("Sound", "Yes");
            SoundState.sprite = SoundOn;
        }
    }


    public void CheckLevelState(int levelIndex)
    {


        switch (levelIndex)
        {
            case 0:
                LevelToggles[0].isOn = true;
                //PlayerPrefs.SetInt("Level", 0);
                break;
            case 1:
                LevelToggles[1].isOn = true;
                //PlayerPrefs.SetInt("Level", 1);
                break;
            case 2:
                LevelToggles[2].isOn = true;
               // PlayerPrefs.SetInt("Level", 2);
                break;
            case 3:
                LevelToggles[3].isOn = true;
                //PlayerPrefs.SetInt("Level", 3);
                break;
            case 4:
                LevelToggles[4].isOn = true;
               // PlayerPrefs.SetInt("Level", 4);
                break;
            case 5:
                LevelToggles[5].isOn = true;
               //PlayerPrefs.SetInt("Level", 5);
                break;
        }

        SetTogglesLevel();


        int level = PlayerPrefs.GetInt("Level") + 1;
        //Debug.Log($"Level = {level}");
    }


    void SetTogglesLevel()
    {
        LevelToggles[0].onValueChanged.AddListener((bool on) =>
        {
            if (on)
            {
                //TODO
                PlayerPrefs.SetInt("Level", 0);
                Debug.Log($"Level = {PlayerPrefs.GetInt("Level") + 1}");
            }
        });

        LevelToggles[1].onValueChanged.AddListener((bool on) =>
        {
            if (on)
            {
                //TODO
                PlayerPrefs.SetInt("Level", 1);
                Debug.Log($"Level = {PlayerPrefs.GetInt("Level") + 1}");
            }
        });

        LevelToggles[2].onValueChanged.AddListener((bool on) =>
        {
            if (on)
            {
                //TODO
                PlayerPrefs.SetInt("Level", 2);
                Debug.Log($"Level = {PlayerPrefs.GetInt("Level") + 1}");
            }
        });

        LevelToggles[3].onValueChanged.AddListener((bool on) =>
        {
            if (on)
            {
                //TODO
                PlayerPrefs.SetInt("Level", 3);
                Debug.Log($"Level = {PlayerPrefs.GetInt("Level") + 1}");
            }
        });

        LevelToggles[4].onValueChanged.AddListener((bool on) =>
        {
            if (on)
            {
                //TODO
                PlayerPrefs.SetInt("Level", 4);
                Debug.Log($"Level = {PlayerPrefs.GetInt("Level") + 1}");
            }
        });

        LevelToggles[5].onValueChanged.AddListener((bool on) =>
        {
            if (on)
            {
                //TODO
                PlayerPrefs.SetInt("Level", 5);
                Debug.Log($"Level = {PlayerPrefs.GetInt("Level") + 1}");
            }
        });
    }



    public void CheckTimerState(int index)
    {
        //Debug.Log($"Time = {PlayerPrefs.GetInt("Timer") + 1} min");

        switch (index)
        {
            case 0:
                TimeToggles[0].isOn = true;
                //PlayerPrefs.SetInt("Timer", 0);
                break;
            case 1:
                TimeToggles[1].isOn = true;
                //PlayerPrefs.SetInt("Timer", 1);
                //Debug.Log($"jjjjmin2");
                break;
            case 2:
                TimeToggles[2].isOn = true;
                //PlayerPrefs.SetInt("Timer", 2);
                //Debug.Log("gaaaa");
                break;
            case 3:
                TimeToggles[3].isOn = true;
                //PlayerPrefs.SetInt("Timer", 3);
                break;
        }
       // Debug.Log($"jjjjmin3");
        SetTogglesTime();


       
       // Debug.Log($"Time = {time} min");
    }


    void SetTogglesTime()
    {
        //Debug.Log($"jjjjmin4");


        TimeToggles[0].onValueChanged.AddListener((bool on) =>
        {
            if (on)
            {
                //TODO
                PlayerPrefs.SetInt("Timer", 0);
                Debug.Log($"Time = {PlayerPrefs.GetInt("Timer") + 1} min");
            }
        });

        TimeToggles[1].onValueChanged.AddListener((bool on) =>
        {
            if (on)
            {
                //TODO
                PlayerPrefs.SetInt("Timer", 1);
                Debug.Log($"Time = {PlayerPrefs.GetInt("Timer") + 1} min");
            }
        });

        TimeToggles[2].onValueChanged.AddListener((bool on) =>
        {
            if (on)
            {
                //TODO
                PlayerPrefs.SetInt("Timer", 2);
                Debug.Log($"Time = {PlayerPrefs.GetInt("Timer") + 1} min");
            }
        });

        TimeToggles[3].onValueChanged.AddListener((bool on) =>
        {
            if (on)
            {
                //TODO
                PlayerPrefs.SetInt("Timer", 3);
                Debug.Log($"Time = {PlayerPrefs.GetInt("Timer") + 1} min");
            }
        });
    }
    public void OnClickCheck()
    {
        int level = PlayerPrefs.GetInt("Level") + 1;
        SceneManager.LoadScene(level);
    }

    public void ChangeMenu()
    {
        if (mainMenu.gameObject.active)
        {
            mainMenu.gameObject.SetActive(false);
            settings.gameObject.SetActive(true);
        }
        else if (!mainMenu.gameObject.active)
        {
            settings.gameObject.SetActive(false);
            mainMenu.gameObject.SetActive(true);
        }
    }

    public void OnClickExit()
    {
        Application.Quit();
    }

    private void Update()
    {

        switch (PlayerPrefs.GetInt("Timer"))
        {
            case 0:
                TimeToggles[0].isOn = true;
               // PlayerPrefs.SetInt("Timer", 0);
                break;
            case 1:
                TimeToggles[1].isOn = true;
               // PlayerPrefs.SetInt("Timer", 1);
                //Debug.Log($"jjjjmin2");
                break;
            case 2:
                TimeToggles[2].isOn = true;
               // PlayerPrefs.SetInt("Timer", 2);
                //Debug.Log("gaaaa");
                break;
            case 3:
                TimeToggles[3].isOn = true;
               // PlayerPrefs.SetInt("Timer", 3);
                break;
        }

        switch (PlayerPrefs.GetInt("Level"))
        {
            case 0:
                LevelToggles[0].isOn = true;
               // PlayerPrefs.SetInt("Level", 0);
                break;
            case 1:
                LevelToggles[1].isOn = true;
               // PlayerPrefs.SetInt("Level", 1);
                break;
            case 2:
                LevelToggles[2].isOn = true;
              //  PlayerPrefs.SetInt("Level", 2);
                break;
            case 3:
                LevelToggles[3].isOn = true;
              //  PlayerPrefs.SetInt("Level", 3);
                break;
            case 4:
                LevelToggles[4].isOn = true;
             //   PlayerPrefs.SetInt("Level", 4);
                break;
            case 5:
                LevelToggles[5].isOn = true;
              //  PlayerPrefs.SetInt("Level", 5);
                break;
        }
    }
}
