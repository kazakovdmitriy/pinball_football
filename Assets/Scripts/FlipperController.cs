﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlipperController : MonoBehaviour
{
    public void Push()
    {
        GetComponent<Rigidbody2D>().AddForce(new Vector2(0, 1) * 12500);
    }
}
