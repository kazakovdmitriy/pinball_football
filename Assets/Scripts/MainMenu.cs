﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MainMenu : MonoBehaviour
{
    [SerializeField] private Text maxScoreBoard;
    [SerializeField] private int maxScore;
    void Start()
    {
        if (PlayerPrefs.HasKey("MAX_SCORE"))
            maxScore = PlayerPrefs.GetInt("MAX_SCORE");
        else PlayerPrefs.SetInt("MAX_SCORE", 0);

        maxScoreBoard.text = maxScore.ToString();
    }

    
    void Update()
    {
        
    }

    public void OnClickPlay1()
    {
        SceneManager.LoadScene(1);
    }

    
}
