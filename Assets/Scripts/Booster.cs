﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Booster : MonoBehaviour
{
    [SerializeField] private GameObject upBorder;
    [SerializeField] private GameObject downBorder;
    [SerializeField] private bool isUpDirection;
    [SerializeField] private Rigidbody2D rgb;
    [SerializeField] private float speed;

    // Update is called once per frame
    void Update()
    {
        if (transform.position.y > upBorder.transform.position.y)
        {
            isUpDirection = false;


        }
        else if (transform.position.y < downBorder.transform.position.y)
        {
            isUpDirection = true;
        }



        rgb.velocity = isUpDirection ? Vector2.up : Vector2.down;
        rgb.velocity *= speed;

    }

  
}
