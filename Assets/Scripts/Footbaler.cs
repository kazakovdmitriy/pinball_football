﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Footbaler : MonoBehaviour
{
    [SerializeField] private GameObject leftBorder;
    [SerializeField] private GameObject rightBorder;
    [SerializeField] private bool isRightDirection;
    [SerializeField] private Rigidbody2D rigidbody;
    [SerializeField] private float speed;
    [SerializeField] private float timeForChangeVector;

    private void Start()
    {
        StartCoroutine(ChangeVector());
    }
    void Update()
    {
        if (transform.position.x > rightBorder.transform.position.x)
        {
            isRightDirection = false;
            

        }
        else if (transform.position.x < leftBorder.transform.position.x)
        {
            isRightDirection = true;
        }

        
        
        rigidbody.velocity = isRightDirection ? Vector2.right : Vector2.left;
        rigidbody.velocity *= speed;
        
    }

    IEnumerator ChangeVector()
    {
        for (; ; )
        {
            var i = Random.Range(-10.0f, 10.0f);
            if (i >= 0)
            {
                isRightDirection = true;
                //Debug.Log(gameObject.name + " Vector change on " + isRightDirection);
            }
            else
            {
                isRightDirection = false;
               // Debug.Log(gameObject.name + " Vector change on " + isRightDirection);
            }
            yield return new WaitForSeconds(timeForChangeVector);
        }
    }
}
