﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MagnetController : MonoBehaviour
{
    public float Radius;
    [Range(0,2f)]
    public float Force;

    public GameObject Ball;

    Rigidbody2D BallRigit;

    private void Start()
    {
        BallRigit = Ball.GetComponent<Rigidbody2D>();
    }
    void FixedUpdate()
    {
        Vector2 ToBall = transform.position - Ball.transform.position;
        if (ToBall.magnitude/ Radius < 0.97f)
        {
            float ActualForce = (1 - ToBall.magnitude / Radius);
            ToBall.Normalize();
            BallRigit.AddForce(ToBall * ActualForce * Force * BallRigit.mass*20);
        }
    }
}
