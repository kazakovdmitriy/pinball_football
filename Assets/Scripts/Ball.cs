﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;



public class Ball : MonoBehaviour
{
    public static Ball Instance { get; private set; }

    [SerializeField] private Rigidbody2D rgbdy;
    [SerializeField] private GameObject lineGoal;
    [SerializeField] private Text goal;
    [SerializeField] private Text scoreLeftText;
    [SerializeField] private Text scoreRightText;
    [SerializeField] private int scoreLeft;
    [SerializeField] private int scoreRight;
    [SerializeField] private float timer = 3f;
    private void Start()
    {
        scoreRight = 0;
        scoreLeft = 0;
        scoreRightText.text = scoreRight.ToString();
        scoreLeftText.text = scoreLeft.ToString();
    }
    void Update()
    {
        if (transform.position.y < -5.5f)
        {
            rgbdy.velocity = Vector3.zero;
            transform.position = new Vector2(3.29f, 3.53f);
            scoreRight++;
            if (scoreRight >= 100 )    
                scoreRight = 0;
            scoreRightText.text = scoreRight.ToString();
            
        }
        if (transform.position.y > 3.94f)
        {
            
            lineGoal.gameObject.SetActive(true);
            goal.gameObject.SetActive(true);
            rgbdy.rotation = 0;
            rgbdy.velocity = Vector3.zero;
            transform.position = new Vector2(transform.position.x, 3.95f);
            if (timer > 0)
            {
                timer -= Time.deltaTime;
            }
            else if (timer <= 0)
            {
                Refresh();
            }

        }
#if UNITY_EDITOR
        if (Input.GetKey(KeyCode.G))
        {
            transform.position = new Vector2(0, 3.95f);

        }
        
#endif
    }
       

    private void Refresh()
    {
       
        transform.position = new Vector2(3.29f, 3.53f);
        scoreLeft++;
        if (scoreLeft >= 100)
            scoreLeft = 0;
        scoreLeftText.text = scoreLeft.ToString();
        lineGoal.gameObject.SetActive(false);
        goal.gameObject.SetActive(false);
        timer = 3f;

        if (PlayerPrefs.HasKey("MAX_SCORE"))
        {
            
            if (PlayerPrefs.GetInt("MAX_SCORE") < scoreLeft)
            {
                PlayerPrefs.SetInt("MAX_SCORE", scoreLeft);
            }
        }
        else PlayerPrefs.SetInt("MAX_SCORE", 0);
    }
}
